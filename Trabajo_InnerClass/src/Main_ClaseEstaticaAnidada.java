
class OtraClaseExterna2 {
	
	static class ClaseInternaAnidada{
		
		public void miMetodo(){
			System.out.println("Esta es mi Clase Interna Anidada");
		}
		
	}
	
}

public class Main_ClaseEstaticaAnidada {

	public static void main(String[] args) {
		
		OtraClaseExterna2.ClaseInternaAnidada internaAnidada = 
				new OtraClaseExterna2.ClaseInternaAnidada();
		
		internaAnidada.miMetodo();

	}

}
