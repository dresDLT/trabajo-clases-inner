
//CLASE EXTERNA
class ClaseExterna {
	int num;
	
	//CLASE INTERNA------------------------------------------------------
	private class ClaseInterna{
		
		private void imprimir(){
			System.out.println("Esta es una Clase Interna");
		}
	}
	
	//-------------------------------------------------------------------
	
	void mostrarClaseInterna(){
		ClaseInterna interna = new ClaseInterna();
		interna.imprimir();
	}
}

public class Main_ClaseInterna{
	public static void main(String args[]){
	      //Instancia de la Clase Externa
	      ClaseExterna externa = new ClaseExterna();
	      //Acceso al metodo mostrarClaseInterna de la Clase Interna
	      externa.mostrarClaseInterna();
	   }
}
