
abstract class ClaseInternaAnonima{
	public abstract void miMetodo();
}


public class Main_ClaseInternaAnonima { //CLASE EXTERNA

	public static void main(String[] args) {
		
		ClaseInternaAnonima internaAnonima = new ClaseInternaAnonima(){

			@Override
			public void miMetodo() {
				System.out.println("Este es un ejemplo de una Clase Interna Anonima");
			}
			
		};

		internaAnonima.miMetodo();
	}

}
