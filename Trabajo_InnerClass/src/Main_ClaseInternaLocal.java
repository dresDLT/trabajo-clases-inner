
//CLASE EXTERNA
class OtraClaseExterna{
	
	public void miMetodo(){ //----------------------------------------------METODO
		
		int num = 27;
		
		//CLASE INTERNA LOCAL------------------------------------------------------
		class ClaseInternaLocal{
			private void imprimir(){
				System.out.println("Este es el m�todo de la Clase Interna Local: "+num);
			}
		}
		//-------------------------------------------------------------------------
		
		//Acceso a la Clase Interna Local
		ClaseInternaLocal internaLocal = new ClaseInternaLocal();
		internaLocal.imprimir();
	}
	
}

public class Main_ClaseInternaLocal {

	public static void main(String[] args) {
		
		OtraClaseExterna externa = new OtraClaseExterna();
		externa.miMetodo();

	}

}
